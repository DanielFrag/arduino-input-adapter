#ifndef InputAdapter_h
#define InputAdapter_h

#include "Arduino.h"
class InputAdapter {
  public:
    InputAdapter(double lMax = 1023, double lMin = 512);
    void convert(double x, double y, double* i, double* j);
  private:
    int _lMin;
    int _lMax;
    double _range;
    double getR1(double x, double y);
    double getMaxHypotenuse(double x, double y);
    double getR2(double x, double y);
    void getRectifiedXY(double, double, double*, double*);
    static double getHypotenuse (double x, double y);
};
#endif
