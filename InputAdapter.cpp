#include "Arduino.h"
#include "InputAdapter.h"

InputAdapter::InputAdapter(double lMax, double lMin) {
  if (lMax < lMin) {
    _lMin = _lMax = _range = 0;
    return;
  }
  _lMin = lMin;
  _lMax = lMax;
  _range = (double) (_lMax - _lMin);
}
double InputAdapter::getHypotenuse(double x, double y) {
  return sqrt(x * x + y * y);
}
// R1 is the ratio between the triangle formed by the input (x, y and the hypotenuse) and the
// triangle formed by the maximum input that are possible in same direction.
double InputAdapter::getR1(double x, double y) {
  double l = x > y ? x : y;
  return l / _range;
}
// Returns the maximum vector module that is possible in the same direction of input.
double InputAdapter::getMaxHypotenuse(double x, double y) {
  return getHypotenuse(x, y) / getR1(x, y);
}
// R2 is the ratio between the triangle formed by the maximum input that are possible (in
// the same direction of input) and the triangle formed by the maximum allowed hypotenuse.
double InputAdapter::getR2(double x, double y) {
  return getMaxHypotenuse(x, y) / _range;
}
// Assume that:
// MaxPossibleHypotenuse / MaxAllowedHypotenuse == inputHypotenuse / rectifiedHypotenuse == R2
// and apply this ratio to each side of the right-triangle, formed by the input,
// to find de adapted input.
void InputAdapter::getRectifiedXY(double x, double y, double* i, double* j) {
  double r2 = getR2(x, y);
  *i = x / r2;
  *j = y / r2;
}
void InputAdapter::convert(double x, double y, double* i, double* j) {
  double newX = constrain(x, _lMin, _lMax);
  double newY = constrain(y, _lMin, _lMax);
  if (newX == 0 && newY == 0) {
    *i = 0;
    *j = 0;
    return;
  }
  getRectifiedXY(newX, newY, i, j);
};