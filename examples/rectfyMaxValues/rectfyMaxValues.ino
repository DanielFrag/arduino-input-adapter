/*
 * Transform the values of an analog input that are based on
 * a square area in values that are based on a circle area.
 * Ex:
 * The * point to value (255, 255)
 *      Analog input      |     Analog converted
 *
 * 255  ____________         255 _
 *     |---------->*|           |  ^-_
 *     |           ^|           |---> *^_
 *     |           ||     to    |     ^ ^|
 *     |           ||           |     |   ^_  
 *     |___________|| 255       |_____|____\ 255
 * 
 * This adapter rectify the inputs (in x and y axis) values in order to produce
 * a limited resultant vector. In the previous example, the resultant vector
 * is limited to 255
*/

#include <InputAdapter.h>

// The InputAdapter has 2 params on its contructor. By default this params are:
// _lMax = 1024
// _lMin = 512
InputAdapter iAdpt(255, 0);

void printData(double x, double y) {
  double i, j; 
  Serial.println("Input: ");
  Serial.print("x: ");
  Serial.println(x);
  Serial.print("y: ");
  Serial.println(y);
  Serial.println("Converted Values: ");
  iAdpt.convert(x, y, &i, &j);
  Serial.print("i: ");
  Serial.println(i);
  Serial.print("j: ");
  Serial.println(j);
  Serial.print("tan x/y | i/j: ");
  if (x == 0 && i == 0) {
    Serial.println("inf");
  } else {
    Serial.print(y/x);
    Serial.print(" | ");
    Serial.println(j/i);
  }
  Serial.println("----------");
}

void setup() {
  Serial.begin(9600);
}

void loop() {
  double i, j;
  // Increase y
  for (double y = 0; y < 255; y++) {
    printData(255, y);
  }
  // Decrease x
  for (double x = 255; x > 0; x--) {
    printData(x, 255);
  }
  delay(2000);
}
